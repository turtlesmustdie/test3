/**
 * Automotive Safety Technologies GmbH
 *
 * @brief      This is the unit for Calculating Obj Relative Velocity.
 * @project    Nanoradar_PPE
 * @file       OBJP_ObjCal_ComVelMod.h
 * @version    MKS: $Revision: 1.5 $
 * @compliance ISO/IEC 9899:1990 (C90), MISRA-C:2012, ISO-26262-6, AutoSar 3.2, VW LAH 893.909
 * @date       MKS: $Date: 2020/05/18 12:31:51CEST $
 * @copyright  Copyright (C) 2020, Automotive Safety Technologies GmbH. All rights reserved.
 * @details    This software unit shall calculate object's relative velocity with respect to Ego velocity.
 *             It also updates the discarded information in the event of any failure (calucation or check).
 * @link       SDRS: doors://doors.wob.vw.vwg:36677/?version=2&prodID=0&urn=urn:telelogic::1-386d3d352cf072ee-M-003670c1
 * @history    -
 **/

#ifndef OBJP_OBJCAL_COMVELMOD_H
#define OBJP_OBJCAL_COMVELMOD_H

/*############################################################################
 # INCLUDES
 ############################################################################*/
#include "ABST_BaseTypes.h"    /* Abstract the base types for this project */
#include "ABST_Types.h"         /* Defines ABST_PI. */
#include "ABST_PrjTypes.h" /* Includes project wide reuseable datatype: tResult, tConfidence */

/*############################################################################
 # DEFINES
 ############################################################################*/
/**
 * @name Version handlers.
 * @details The file version should be added here for interface compability check.
 * @{
 */
#define MAJOR_VERSION_OBJP_OBJCAL_COMVELMOD_H (1)
#define MINOR_VERSION_OBJP_OBJCAL_COMVELMOD_H (3)
#define PATCH_VERSION_OBJP_OBJCAL_COMVELMOD_H (0)
 /** @} */

 /**
  * @brief   Minimum Maximum range of object properties.
  * @details The Minimum and Maximum range of object properties will be represented here.
  * @Note: Unit is in SI sytem ie. meter, second, Kg...
  *        In order to avoid decimal arithmatic, for implemetaion Meter is converted to Cm and the mm resolution is dropped.
  */
#define MIN_OBJ_ABSOLUTE_VELOCITYX               (-12800)            /** Req:  IRS_SWE1_79  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */
#define MAX_OBJ_ABSOLUTE_VELOCITYX               ( 12787)            /** Req:  IRS_SWE1_79  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */
#define MIN_EGO_ABSOLUTE_VELOCITYX               (-12778)            /** Req:  IRS_SWE1_59  range: -127.7874 .. 127.791 Unit: MeterPerSecon Safety: QM */
#define MAX_EGO_ABSOLUTE_VELOCITYX               ( 12779)            /** Req:  IRS_SWE1_59  range: -127.7874 .. 127.791 Unit: MeterPerSecon Safety: QM */
#define MIN_OBJ_RELATIVE_VELOCITYX               (-12800)            /** Req:  IRS_SWE1_108  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */
#define MAX_OBJ_RELATIVE_VELOCITYX               ( 12787)            /** Req:  IRS_SWE1_108  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */


/*############################################################################
 # TYPE DEFINITIONS
 ############################################################################*/

/**
 * @brief   Type to structure input signals of cyclic process function.
 * @details This structure do contain the ego and object kinematics.
 */
typedef struct
{
	tInt16                    i16AbsoluteVelocityX;                /** Req:  IRS_SWE1_79  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */
    tInt16                    i16AbsoluteEgoVelocityX;             /** Req:  IRS_SWE1_59  range: -128 .. 127.875 Unit: MeterPerSecon Safety: QM */
} tComVelModInput;

/**
 * @brief   Type to structure output signals of cyclic process function.
 * @details This structure do contrain the criticality.
 */
typedef struct
{
    tInt16                    i16RelativeVelocityX;                /** Req:  IRS_SWE1_108  range: -128 .. 127.875 Unit: MeterPerSecon Default: 0 Safety: QM*/
    tBool                     bDiscardedState;                      /** Req:  IRS_SWE1_107  range: True .. False Unit: None Default: True Safety: QM */
} tComVelModOutput;

/*############################################################################
 # GLOBAL VARIABLES
 ############################################################################*/
/**
 * @brief   Memory for software unit initialization state.
 * @details This global variable shall be used to hold the software unit initialization state to access them during analysis and tests.
 */
extern tInitState g_ComVelMod_eInitState; /** Enum: 'NotInitialized','Initialized' Default: 'NotInitialized' Error: 'NotInitialized' Unit: - Safety: QM */

/*############################################################################
 # FUNCTION PROTOTYPES
 ############################################################################*/
/**
 * @brief      Initialization function of OBJP OBJP_ObjCal_ComVelMod.
 * @details    This function shall be called to initialize or reinitialize this unit.
 * @param      Void.
 * @retval     ERR_OK: No error are detected.
 */
tResult Objp_ObjCal_ComVelMod_Init(void);

/**
 * @brief      Process function of OBJP_ObjCal_ComVelMod.
 * @details    This function shall be called to execute the main functional process.
 * @param[in]  i_sObjPCvmInput: Structure of all input signals of the process function.
 * @param[out] o_sObjPCvmOutput: Structure output signals of the process function.
 * @retval     ERR_OK: No error are detected.
 * @retval     ERR_FATAL: A error which stops the execution are detected.
 * @retval     ERR_NOT_IMPL: Still now not implemented.
 */
tResult Objp_ObjCal_ComVelMod_Process(const tComVelModInput*  const i_psComVelModInput,
                                            tComVelModOutput* const o_psComVelModOutput);

#endif /* OBJP_OBJCAL_COMVELMOD_H */

/* EOF */
